let paises =
[
    {
        "pais": "Estados Unidos",
        "moneda": "Dólar estadounidense",
        "valor_cambio": 1.00
    },
    {
        "pais": "España",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Japón",
        "moneda": "Yen",
        "valor_cambio": 110.62
    },
    {
        "pais": "Reino Unido",
        "moneda": "Libra esterlina",
        "valor_cambio": 0.74
    },
    {
        "pais": "Canadá",
        "moneda": "Dólar canadiense",
        "valor_cambio": 1.28
    },
    {
        "pais": "Australia",
        "moneda": "Dólar australiano",
        "valor_cambio": 1.36
    },
    {
        "pais": "China",
        "moneda": "Yuan",
        "valor_cambio": 6.37
    },
    {
        "pais": "India",
        "moneda": "Rupia india",
        "valor_cambio": 74.92
    },
    {
        "pais": "Brasil",
        "moneda": "Real brasileño",
        "valor_cambio": 5.35
    },
    {
        "pais": "México",
        "moneda": "Peso mexicano",
        "valor_cambio": 20.17
    },
    {
        "pais": "Alemania",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Francia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Italia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Rusia",
        "moneda": "Rublo ruso",
        "valor_cambio": 80.31
    },
    {
        "pais": "Sudáfrica",
        "moneda": "Rand",
        "valor_cambio": 14.88
    }
];

let slider = document.getElementById('selector');

let InputPais=document.getElementById('pais');
let InputMoneda=document.getElementById('moneda');
let InputCambio=document.getElementById('valor');

slider.addEventListener('change', function(){

        valorSeleccionado= slider.value;

    if(valorSeleccionado==0){
        pais = paises[0].pais;
        moneda = paises[0].moneda;
        cambio = paises[0].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==1){
        pais = paises[1].pais;
        moneda = paises[1].moneda;
        cambio = paises[1].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==2){
        pais = paises[2].pais;
        moneda = paises[2].moneda;
        cambio = paises[2].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }

    if(valorSeleccionado==3){
        pais = paises[3].pais;
        moneda = paises[3].moneda;
        cambio = paises[3].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }

    if(valorSeleccionado==4){
        pais = paises[4].pais;
        moneda = paises[4].moneda;
        cambio = paises[4].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==5){
        pais = paises[5].pais;
        moneda = paises[5].moneda;
        cambio = paises[5].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==6){
        pais = paises[6].pais;
        moneda = paises[6].moneda;
        cambio = paises[6].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==7){
        pais = paises[7].pais;
        moneda = paises[7].moneda;
        cambio = paises[7].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==8){
        pais = paises[8].pais;
        moneda = paises[8].moneda;
        cambio = paises[8].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==9){
        pais = paises[9].pais;
        moneda = paises[9].moneda;
        cambio = paises[9].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==10){
        pais = paises[10].pais;
        moneda = paises[10].moneda;
        cambio = paises[10].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==11){
        pais = paises[11].pais;
        moneda = paises[11].moneda;
        cambio = paises[11].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==12){
        pais = paises[12].pais;
        moneda = paises[12].moneda;
        cambio = paises[12].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==13){
        pais = paises[13].pais;
        moneda = paises[13].moneda;
        cambio = paises[13].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==14){
        pais = paises[14].pais;
        moneda = paises[14].moneda;
        cambio = paises[14].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
    if(valorSeleccionado==15){
        pais = paises[15].pais;
        moneda = paises[15].moneda;
        cambio = paises[15].valor_cambio;
        InputPais.value=pais;
        InputMoneda.value=moneda;
        InputCambio.value=cambio;
    }
})

let pais = "";
let moneda= "";
let cambio= "";


